// take 2 inputs and add them together, give output

import java.util.Scanner;

public class Addition
{
	public static void main(String[] args)
	{
	  Scanner input = new Scanner(System.in);
	  
	  int number1;
	  int number2;
	  int sum;
	  
	  //prompt user to enter first integer
	  System.out.println("Enter first integer: ");
	  number1 = input.nextInt();
	  
	  //prompt user to enter second integer
	  System.out.println("Enter second integer: ");
	  number2 = input.nextInt();
	  
	  sum = number1 + number2;
	  
	  System.out.printf("The sum of the two integers is %d \n ", sum);

	} // end method main

} // end class Addition
